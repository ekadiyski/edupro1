-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Време на генериране: 13 юли 2017 в 20:36
-- Версия на сървъра: 5.5.54-38.7-log
-- Версия на PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- БД: `vratsad_edupro`
--

-- --------------------------------------------------------

--
-- Структура на таблица `cl_attestation_criterias`
--

CREATE TABLE IF NOT EXISTS `cl_attestation_criterias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(90) NOT NULL,
  `max_points` tinyint(5) NOT NULL,
  `sequence` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sequence` (`sequence`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Схема на данните от таблица `cl_attestation_criterias`
--

INSERT INTO `cl_attestation_criterias` (`id`, `name`, `max_points`, `sequence`) VALUES
(1, 'Планиране', 12, 1),
(2, 'Управление на групата/класа', 14, 2),
(3, 'Стратегии/методи на преподаване', 6, 3),
(4, 'Оценяване/ориентираност към резултати', 8, 4),
(5, 'Принос към общността', 22, 5),
(6, 'Познаване и прилагане на нормативната уредба', 4, 6),
(7, 'Други професионални изисквания. Личностни качества', 14, 7),
(8, 'Показател на училището 1', 8, 8),
(9, 'Показател на училището 2', 12, 9);

-- --------------------------------------------------------

--
-- Структура на таблица `cl_portfolio_types`
--

CREATE TABLE IF NOT EXISTS `cl_portfolio_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(90) NOT NULL,
  `description` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Схема на данните от таблица `cl_portfolio_types`
--

INSERT INTO `cl_portfolio_types` (`id`, `name`, `description`) VALUES
(1, 'Дипломи за висше образование', ''),
(2, 'Сертификати от обучения', ''),
(3, 'Грамота от олимпиада', ''),
(4, 'Награда', '');

-- --------------------------------------------------------

--
-- Структура на таблица `cl_trainings`
--

CREATE TABLE IF NOT EXISTS `cl_trainings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `training` varchar(200) NOT NULL,
  `description` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Схема на данните от таблица `cl_trainings`
--

INSERT INTO `cl_trainings` (`id`, `training`, `description`) VALUES
(1, 'Обучение 1', 'Не е за изпускане'),
(2, 'Обучение 2', 'Който разбира тук се спира!');

-- --------------------------------------------------------

--
-- Структура на таблица `cm_attestation_cards`
--

CREATE TABLE IF NOT EXISTS `cm_attestation_cards` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `teacher_id` int(10) unsigned NOT NULL,
  `mentor_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher_id` (`teacher_id`),
  KEY `mentor_id` (`mentor_id`),
  KEY `created_by` (`created_by`),
  KEY `deleted_by` (`deleted_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=18 ;

--
-- Схема на данните от таблица `cm_attestation_cards`
--

INSERT INTO `cm_attestation_cards` (`id`, `teacher_id`, `mentor_id`, `created_at`, `created_by`, `updated_at`, `deleted_at`, `deleted_by`) VALUES
(1, 2, 3, '2017-06-02 00:00:00', 2, '0000-00-00 00:00:00', NULL, NULL),
(2, 1, 3, '2017-06-04 06:10:06', 1, '2017-06-04 06:10:06', NULL, NULL),
(3, 1, 3, '2017-06-04 06:10:39', 1, '2017-06-04 06:10:39', NULL, NULL),
(4, 1, 3, '2017-06-04 06:11:03', 1, '2017-06-04 06:11:03', NULL, NULL),
(5, 1, 3, '2017-06-04 07:37:04', 1, '2017-06-04 07:37:04', NULL, NULL),
(6, 1, 3, '2017-06-04 07:37:59', 1, '2017-06-04 07:37:59', NULL, NULL),
(7, 1, 3, '2017-06-04 07:40:22', 1, '2017-06-04 07:40:22', NULL, NULL),
(8, 1, 3, '2017-06-04 07:40:35', 1, '2017-06-04 07:40:35', NULL, NULL),
(9, 1, 3, '2017-06-04 07:40:45', 1, '2017-06-04 07:40:45', NULL, NULL),
(10, 1, 3, '2017-06-04 07:41:15', 1, '2017-06-04 07:41:15', NULL, NULL),
(11, 1, 3, '2017-06-04 07:41:50', 1, '2017-06-04 07:41:50', NULL, NULL),
(12, 1, 3, '2017-06-04 07:42:19', 1, '2017-06-04 07:42:19', NULL, NULL),
(13, 1, 3, '2017-06-04 07:44:06', 1, '2017-06-04 07:44:06', NULL, NULL),
(14, 1, 3, '2017-06-04 07:46:25', 1, '2017-06-04 07:46:25', NULL, NULL),
(15, 1, 3, '2017-06-04 09:15:16', 1, '2017-06-04 09:15:16', NULL, NULL),
(16, 1, 3, '2017-06-04 09:15:29', 1, '2017-06-04 09:15:29', NULL, NULL),
(17, 1, 3, '2017-06-16 13:38:18', 1, '2017-06-16 13:38:18', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура на таблица `cm_attestation_items`
--

CREATE TABLE IF NOT EXISTS `cm_attestation_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cm_attestation_card_id` int(10) unsigned NOT NULL,
  `cl_attestation_criterion_id` int(10) unsigned NOT NULL,
  `teacher_pts` tinyint(5) NOT NULL,
  `teacher_comments` varchar(300) DEFAULT NULL,
  `mentor_pts` int(11) DEFAULT NULL,
  `mentor_comments` varchar(300) DEFAULT NULL,
  `recommendations` varchar(300) DEFAULT NULL,
  `cl_training_id` int(10) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(10) unsigned NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cm_attestation_card_id` (`cm_attestation_card_id`),
  KEY `cl_attestation_criterion_id` (`cl_attestation_criterion_id`),
  KEY `cl_training_id` (`cl_training_id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  KEY `deleted_by` (`deleted_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Схема на данните от таблица `cm_attestation_items`
--

INSERT INTO `cm_attestation_items` (`id`, `cm_attestation_card_id`, `cl_attestation_criterion_id`, `teacher_pts`, `teacher_comments`, `mentor_pts`, `mentor_comments`, `recommendations`, `cl_training_id`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
(3, 1, 1, 5, 'Добър съм в тази работа ... и те така те.', 0, '', '', NULL, '2017-06-02 00:00:00', 2, '0000-00-00 00:00:00', 2, NULL, NULL),
(4, 13, 1, 10, 'асд', NULL, NULL, NULL, NULL, '2017-06-04 07:44:06', 1, '2017-06-04 07:44:06', 1, NULL, NULL),
(5, 13, 2, 9, 'фгх', NULL, NULL, NULL, NULL, '2017-06-04 07:44:06', 1, '2017-06-04 07:44:06', 1, NULL, NULL),
(6, 14, 1, 12, 'аз', NULL, NULL, NULL, NULL, '2017-06-04 07:46:25', 1, '2017-06-04 07:46:25', 1, NULL, NULL),
(7, 14, 2, 11, 'мога', NULL, NULL, NULL, NULL, '2017-06-04 07:46:25', 1, '2017-06-04 07:46:25', 1, NULL, NULL),
(8, 14, 3, 10, 'а', NULL, NULL, NULL, NULL, '2017-06-04 07:46:25', 1, '2017-06-04 07:46:25', 1, NULL, NULL),
(9, 14, 4, 8, 'ти', NULL, NULL, NULL, NULL, '2017-06-04 07:46:25', 1, '2017-06-04 07:46:25', 1, NULL, NULL),
(10, 14, 5, 8, '?', NULL, NULL, NULL, NULL, '2017-06-04 07:46:25', 1, '2017-06-04 07:46:25', 1, NULL, NULL),
(11, 14, 6, 8, 'ф', NULL, NULL, NULL, NULL, '2017-06-04 07:46:25', 1, '2017-06-04 07:46:25', 1, NULL, NULL),
(12, 14, 7, 7, 'д', NULL, NULL, NULL, NULL, '2017-06-04 07:46:25', 1, '2017-06-04 07:46:25', 1, NULL, NULL),
(13, 14, 8, 6, 's', NULL, NULL, NULL, NULL, '2017-06-04 07:46:25', 1, '2017-06-04 07:46:25', 1, NULL, NULL),
(14, 14, 9, 5, 'a', NULL, NULL, NULL, NULL, '2017-06-04 07:46:25', 1, '2017-06-04 07:46:25', 1, NULL, NULL),
(15, 16, 1, 1, '', NULL, NULL, NULL, NULL, '2017-06-04 09:15:29', 1, '2017-06-04 09:15:29', 1, NULL, NULL),
(16, 16, 2, 2, '', NULL, NULL, NULL, NULL, '2017-06-04 09:15:29', 1, '2017-06-04 09:15:29', 1, NULL, NULL),
(17, 16, 3, 3, '', NULL, NULL, NULL, NULL, '2017-06-04 09:15:29', 1, '2017-06-04 09:15:29', 1, NULL, NULL),
(18, 16, 4, 4, '', NULL, NULL, NULL, NULL, '2017-06-04 09:15:29', 1, '2017-06-04 09:15:29', 1, NULL, NULL),
(19, 16, 5, 5, '', NULL, NULL, NULL, NULL, '2017-06-04 09:15:29', 1, '2017-06-04 09:15:29', 1, NULL, NULL),
(20, 16, 6, 6, '', NULL, NULL, NULL, NULL, '2017-06-04 09:15:29', 1, '2017-06-04 09:15:29', 1, NULL, NULL),
(21, 16, 7, 7, '', NULL, NULL, NULL, NULL, '2017-06-04 09:15:29', 1, '2017-06-04 09:15:29', 1, NULL, NULL),
(22, 16, 8, 8, '', NULL, NULL, NULL, NULL, '2017-06-04 09:15:29', 1, '2017-06-04 09:15:29', 1, NULL, NULL),
(23, 16, 9, 100, '', NULL, NULL, NULL, NULL, '2017-06-04 09:15:29', 1, '2017-06-04 09:15:29', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура на таблица `cm_goals`
--

CREATE TABLE IF NOT EXISTS `cm_goals` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `deadline` datetime NOT NULL,
  `cm_attestation_item_id` int(10) unsigned DEFAULT NULL,
  `is_finished` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'mentor approves completion of each goal',
  `cl_training_id` int(11) unsigned NOT NULL,
  `mentor_id` int(11) unsigned NOT NULL,
  `goal_owner_id` int(11) unsigned NOT NULL COMMENT 'user_id for the user that this goal is about',
  `created_at` datetime NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `teacher_grade` int(11) DEFAULT NULL,
  `teacher_comment` varchar(500) DEFAULT NULL,
  `cm_portfolio_id` int(10) unsigned DEFAULT NULL,
  `mentor_grade` int(11) DEFAULT NULL,
  `mentor_comment` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cm_attestation_item_id` (`cm_attestation_item_id`),
  KEY `cm_portfolio_id` (`cm_portfolio_id`),
  KEY `fk_goal_training_id` (`cl_training_id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  KEY `deleted_by` (`deleted_by`),
  KEY `goal_owner` (`goal_owner_id`),
  KEY `fk_goal_menor_id` (`mentor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Схема на данните от таблица `cm_goals`
--

INSERT INTO `cm_goals` (`id`, `title`, `deadline`, `cm_attestation_item_id`, `is_finished`, `cl_training_id`, `mentor_id`, `goal_owner_id`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `teacher_grade`, `teacher_comment`, `cm_portfolio_id`, `mentor_grade`, `mentor_comment`) VALUES
(2, 'Да стана дуговен водач', '2017-12-30 00:00:00', 3, 0, 2, 3, 2, '0000-00-00 00:00:00', 2, '0000-00-00 00:00:00', 2, '0000-00-00 00:00:00', 2, 0, '', NULL, 0, ''),
(3, 'Целта миии', '2017-06-08 00:00:00', 3, 0, 1, 2, 2, '2017-06-04 05:45:30', 2, '2017-06-04 05:45:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'хоп23', '2017-06-20 00:00:00', 3, 0, 1, 2, 2, '2017-06-04 09:05:08', 2, '2017-06-16 13:41:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура на таблица `cm_goal_comments`
--

CREATE TABLE IF NOT EXISTS `cm_goal_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cm_goal_id` int(11) unsigned NOT NULL,
  `comment` varchar(300) NOT NULL,
  `is_seen` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `cm_goal_id` (`cm_goal_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Схема на данните от таблица `cm_goal_comments`
--

INSERT INTO `cm_goal_comments` (`id`, `cm_goal_id`, `comment`, `is_seen`, `created_at`, `created_by`, `updated_at`) VALUES
(1, 2, 'Добре си го казал. Ама започни с нещо по-реастично. Например да постигнеш дзен мир.', 0, '2017-06-02 00:00:00', 3, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура на таблица `cm_portfolios`
--

CREATE TABLE IF NOT EXISTS `cm_portfolios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `file` varchar(200) DEFAULT NULL COMMENT 'path to attached file',
  `is_link` tinyint(1) DEFAULT '0',
  `title` varchar(90) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `cl_portfolio_types_id` int(11) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) unsigned NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `period` varchar(100) DEFAULT NULL,
  `study_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `cl_portfolio_types_id` (`cl_portfolio_types_id`),
  KEY `deleted_by` (`deleted_by`),
  KEY `updated_by` (`updated_by`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Схема на данните от таблица `cm_portfolios`
--

INSERT INTO `cm_portfolios` (`id`, `user_id`, `file`, `is_link`, `title`, `description`, `cl_portfolio_types_id`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `period`, `study_type`) VALUES
(1, 2, 'var/www/edupro1/certificate132', 0, 'Social Entrepreneurship', NULL, 2, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, NULL),
(2, 2, 'https://images.template.net/wp-content/uploads/2015/06/Course-Completion-Certificate-Template.jpg', 1, 'My best', 'I did my best', 3, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, NULL, NULL),
(3, 2, 'https://facebook.github.io/react/docs/forms.html', 1, 'пробно', 'bla bla', 2, '2017-06-04 09:14:27', 2, '2017-06-04 09:14:27', 0, NULL, NULL, '3 Месеца', 'такова'),
(4, 2, 'фолио', 1, 'порт', 'от СУ', 1, '2017-06-16 11:50:03', 2, '2017-06-16 11:50:03', 0, NULL, NULL, '6 Години', 'магистър');

-- --------------------------------------------------------

--
-- Структура на таблица `cm_teacher_mentors`
--

CREATE TABLE IF NOT EXISTS `cm_teacher_mentors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(10) unsigned NOT NULL,
  `mentor_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher_id` (`teacher_id`),
  KEY `mentor_id` (`mentor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Схема на данните от таблица `cm_teacher_mentors`
--

INSERT INTO `cm_teacher_mentors` (`id`, `teacher_id`, `mentor_id`) VALUES
(3, 2, 3);

-- --------------------------------------------------------

--
-- Структура на таблица `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `first_name` varchar(90) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(90) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_mentor` tinyint(1) NOT NULL DEFAULT '0',
  `cl_subject_id` int(11) DEFAULT NULL,
  `user_type` enum('admin','teacher','','') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'teacher',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `cl_subject_id` (`cl_subject_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Схема на данните от таблица `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `first_name`, `last_name`, `phone`, `is_mentor`, `cl_subject_id`, `user_type`) VALUES
(1, 'user123', 'user12@mail.bg', 'asdf1234(hashed)', NULL, NULL, '2017-06-03 10:33:20', 'User', 'Twelve', '', 0, 0, 'admin'),
(2, 'teacher1', 'teach@like.champ', 'pass-teach', NULL, NULL, NULL, 'Gospodin', 'Ivanov', NULL, 0, 0, 'admin'),
(3, 'mentor1', 'mentora@mi.bee', 'mentorirai', NULL, NULL, NULL, 'Mentor', 'Popov', NULL, 1, 0, 'admin'),
(5, 'user100', 'stoler@sam.olet', '$2y$10$w2jlU.7QcNWIcICMTjjS4OcAdGHjY13MZM.HqSC8eCYq7ibDULLh6', NULL, '2017-06-03 10:33:48', '2017-06-03 10:33:48', 'Stolet', 'Stamatov', '09898798', 0, NULL, 'teacher');

--
-- Ограничения за дъмпнати таблици
--

--
-- Ограничения за таблица `cm_attestation_cards`
--
ALTER TABLE `cm_attestation_cards`
  ADD CONSTRAINT `fk_attestation_card_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_attestation_card_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_attestation_mentor_id` FOREIGN KEY (`mentor_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_attestation_teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `users` (`id`);

--
-- Ограничения за таблица `cm_attestation_items`
--
ALTER TABLE `cm_attestation_items`
  ADD CONSTRAINT `fk_attestation_card_id` FOREIGN KEY (`cm_attestation_card_id`) REFERENCES `cm_attestation_cards` (`id`),
  ADD CONSTRAINT `fk_attestation_criterion_id` FOREIGN KEY (`cl_attestation_criterion_id`) REFERENCES `cl_attestation_criterias` (`id`),
  ADD CONSTRAINT `fk_attestation_item_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_attestation_item_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_attestation_item_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_training_id2` FOREIGN KEY (`cl_training_id`) REFERENCES `cl_trainings` (`id`);

--
-- Ограничения за таблица `cm_goals`
--
ALTER TABLE `cm_goals`
  ADD CONSTRAINT `fk_goal_attestation_id` FOREIGN KEY (`cm_attestation_item_id`) REFERENCES `cm_attestation_items` (`id`),
  ADD CONSTRAINT `fk_goal_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_goal_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_goal_menor_id` FOREIGN KEY (`mentor_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_goal_owner_id` FOREIGN KEY (`goal_owner_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_goal_portfolio_id` FOREIGN KEY (`cm_portfolio_id`) REFERENCES `cm_portfolios` (`id`),
  ADD CONSTRAINT `fk_goal_training_id` FOREIGN KEY (`cl_training_id`) REFERENCES `cl_trainings` (`id`),
  ADD CONSTRAINT `fk_goal_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Ограничения за таблица `cm_goal_comments`
--
ALTER TABLE `cm_goal_comments`
  ADD CONSTRAINT `fk_goal_comment_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_goal_id` FOREIGN KEY (`cm_goal_id`) REFERENCES `cm_goals` (`id`);

--
-- Ограничения за таблица `cm_portfolios`
--
ALTER TABLE `cm_portfolios`
  ADD CONSTRAINT `fk_portfolio_type_id` FOREIGN KEY (`cl_portfolio_types_id`) REFERENCES `cl_portfolio_types` (`id`),
  ADD CONSTRAINT `fk_user_portfolio_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения за таблица `cm_teacher_mentors`
--
ALTER TABLE `cm_teacher_mentors`
  ADD CONSTRAINT `fk_mentor_id` FOREIGN KEY (`mentor_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
