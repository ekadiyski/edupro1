<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClAttestationCriteria extends Model
{
    protected $fillable = [
        'name', 'max_pts', 'sequence'
    ];
}
