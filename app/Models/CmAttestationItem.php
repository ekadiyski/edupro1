<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CmAttestationItem extends Model
{
    protected $fillable = [
        'cm_attestation_card_id', 'cl_attestation_criterion_id', 
        'teacher_pts', 'teacher_comments', 
        'mentor_pts', 'mentor_comments', 
        'recommendations', 'cl_training_id', 
        'created_by', 'updated_by'
    ];

    public function cmAttestationCard(){
        return $this->belongsTo('App\Models\CmAttestationCard');
    }

    public function clAttestationCriteria(){
        return $this->belongsTo('App\Models\ClAttestationCriteria', 'cl_attestation_criterion_id', 'id');
    }
}
