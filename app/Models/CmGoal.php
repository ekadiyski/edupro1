<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CmGoal extends Model
{
    protected $table = 'cm_goals';

    protected $fillable = [
        'title', 
        'deadline', 
        'cm_attestation_item_id',
        'is_finished', 
        'cl_training_id',
        'mentor_id',
        'goal_owner_id',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
        'teacher_grade',
        'teacher_comment',
        'cm_portfolio_id',
        'mentor_grade',
        'mentor_comment'
    ];
}
