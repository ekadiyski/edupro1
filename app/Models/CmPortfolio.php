<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CmPortfolio extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'file', 'description', 'portfolio_types_id', 'period','study_type','user_id',
        'is_link','cl_portfolio_types_id','created_by','updated_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */


    public function portfolio_types(){
        return $this->hasMany('App\Models\ClPortfolioTypes');
    }

}
