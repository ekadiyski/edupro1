<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CmAttestationCard extends Model
{
    protected $fillable = [
        'teacher_id', 'mentor_id', 'created_by'
    ];

    public function teacher(){
        return $this->belongsTo('App\Models\User', 'teacher_id');
    }

	public function mentor(){
        return $this->belongsTo('App\Models\User', 'mentor_id');
    }    
}
