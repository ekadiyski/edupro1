<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClPortfolioTypes extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function portfolio(){
        return $this->belongsTo('App\Models\CmPortfolio');;
    }

}