<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CmAttestationCard;
use App\Models\CmAttestationItem;
use App\Models\User;
use App\Repository\Transformers\CmAttestationCardTransformer;
use App\Repository\Transformers\CmAttestationItemTransformer;
use App\Http\Controllers\ApiController;
use \Illuminate\Http\Response as Res;
use Illuminate\Support\Facades\Input;
use Validator;

class CmAttestationsController extends ApiController
{
    protected $cm_attestation_card_transformer;
    protected $item_transformer;

    public function __construct(CmAttestationCardTransformer $attestation_card_transformer, CmAttestationItemTransformer $attestation_item_transformer)
    {
        $this->cm_attestation_card_transformer = $attestation_card_transformer;
        $this->item_transformer = $attestation_item_transformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cm_attestation_cards = CmAttestationCard::with(['teacher', 'mentor'])->get();

        //$cm_attestation_cards = CmAttestationCard::paginate($limit);
        return $this->respond([
                'status' => 'success',
                'status_code' => Res::HTTP_OK,
                'message' => 'Records Found!',
                'cm_attestation_cards' => $this->cm_attestation_card_transformer->transformCollection($cm_attestation_cards->all())
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array (
              'teacher_pts' => 'required',
              // 'mentor_id' => 'required'
        );

        $errors = '';

        $validator = Validator::make($request->all(), $rules);

        if ($validator-> fails()){

          $messages = $validator->errors()->getMessages();
          foreach($messages as $message){
            foreach($message as $msg){
              $errors .= '<p>'.$msg.'</p>';
            }
          }

          return $this->respondValidationError('Неуспешен опит.', $errors);

        }

        else{
            $cm_attestation_card = CmAttestationCard::create([
                'teacher_id' => 1,
                'mentor_id' => 3,//TODO: get it from CmTeacherMentors
                'created_by' => 1
            ]);

            $criterias_count = count($request['teacher_pts']);

            for($i = 0; $i < $criterias_count; $i++)
            {
                $cm_attestation_item = CmAttestationItem::create([
                    'cm_attestation_card_id' => $cm_attestation_card->id,
                    'cl_attestation_criterion_id' => $request->cl_attestation_criterion_id[$i],//TODO: get it from CmTeacherMentors
                    'teacher_pts' => $request->teacher_pts[$i],
                    'teacher_comments' => $request->teacher_comments[$i],
                    'created_by' => 1,
                    'updated_by' => 1
                ]);
            }

            return $this->respond([

                'status' => 'success',
                'status_code' => Res::HTTP_CREATED,
                'message' => 'Успешно създадена атестация',
                'cm_attestation_card' => $this->cm_attestation_card_transformer->transform($cm_attestation_card)
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $atestation = CmAttestationItem::find($id);

    return $this->respond([
        'status' => 'success',
        'status_code' => Res::HTTP_OK,
        'atestation' => $this->CmAttestationItemTransformer->transform($atestation)
    ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
         $rules = array (

          // 'title' => 'required',
      );

      $errors = '';

      $validator = Validator::make($request->all(), $rules);

      if ($validator-> fails()){

        $messages = $validator->errors()->getMessages();
        foreach($messages as $message){
          foreach($message as $msg){
            $errors .= '<p>'.$msg.'</p>';
          }
        }

        return $this->respondValidationError('Грешка в попълнването на полетата!', $errors);

      }

      else{

        $criterias_count = count($request['teacher_pts']);

            for($i = 0; $i < $criterias_count; $i++)
            {
                $cm_attestation_item = CmAttestationItem::where('cm_attestation_card_id', '=', $request['attestation_id'])
          ->update([
                    'teacher_pts' => $request->teacher_pts[$i],
                    'teacher_comments' => $request->teacher_comments[$i],
                    'created_by' => 1,
                    'updated_by' => 1
                ]);
            }

          return $this->respond([

              'status' => 'success',
              'status_code' => Res::HTTP_OK,
              'message' => 'Успешно редактирахте атестация!',
          ]);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request){

    $id = $request['cm_attestation_card_id'];

    $att = CmAttestationItem::where('cm_attestation_card_id', '=', $id)->delete();
    $card = CmAttestationCard::find($id)->delete();
  


    return $this->respond([
        'status' => 'success',
        'status_code' => Res::HTTP_OK,
        'message' => 'Успешно изтрито Портфолио!',
    ]);

  }

    public function get_items($attestation_id)
    { 
        $cm_attestation_items = CmAttestationItem::with('clAttestationCriteria')->where('cm_attestation_card_id', $attestation_id)->get();

        return $this->respond([
                'status' => 'success',
                'status_code' => Res::HTTP_OK,
                'message' => 'Records Found!',
                'cm_attestation_items' => $this->item_transformer->transformCollection($cm_attestation_items->all())
            ]);
    }
}
