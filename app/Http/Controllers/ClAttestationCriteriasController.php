<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ClAttestationCriteria;
use App\Repository\Transformers\ClAttestationCriteriasTransformer;
use App\Http\Controllers\ApiController;
use \Illuminate\Http\Response as Res;
use Illuminate\Support\Facades\Input;
use Validator;

class ClAttestationCriteriasController extends ApiController
{
    protected $cm_attestation_criterias_transformer;

    public function __construct(ClAttestationCriteriasTransformer $attestation_criterias_transformer)
    {
        $this->cm_attestation_criterias_transformer = $attestation_criterias_transformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $limit = Input::get('limit') ?: 20;

        $cl_attestation_criterias = ClAttestationCriteria::paginate($limit);

        return $this->respondWithPagination($cl_attestation_criterias, [
          'cl_attestation_criterias' => $this->cm_attestation_criterias_transformer->transformCollection($cl_attestation_criterias->all())
        ], 'Records Found!');   
    }
}
