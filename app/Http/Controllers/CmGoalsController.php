<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CmGoal;
use App\Repository\Transformers\GoalTransformer;
use App\Http\Controllers\ApiController;
use \Illuminate\Http\Response as Res;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;

class CmGoalsController extends ApiController
{

  protected $goalTransformer;

  public function __construct(GoalTransformer $goalTransformer){

    $this->goalTransformer = $goalTransformer;

  }

  public function index(){
    $limit = Input::get('limit') ?: 20;

    $goals = CmGoal::paginate($limit);

    return $this->respondWithPagination($goals, [
      'goals' => $this->goalTransformer->transformCollection($goals->all())
    ], 'Records Found!');
  }

  public function show($id){

    $goal = CmGoal::find($id);

    return $this->respond([
        'status' => 'success',
        'status_code' => Res::HTTP_OK,
        'goal' => $this->goalTransformer->transform($goal)
    ]);
  }

  public function store(Request $request)
  {
      $rules = array (

          'title' => 'required',
            'deadline' => 'required|date',
            'cl_training_id' => 'required|integer',
            'cm_attestation_item_id' => 'required|integer',

      );

      $errors = '';

      $validator = Validator::make($request->all(), $rules);

      if ($validator-> fails()){

        $messages = $validator->errors()->getMessages();
        foreach($messages as $message){
          foreach($message as $msg){
            $errors .= '<p>'.$msg.'</p>';
          }
        }

        return $this->respondValidationError('Проблем с валидацията.', $errors);

      }

      else{

          $goal = CmGoal::create([

              'title' => $request['title'],
              'deadline' => $request['deadline'],
              'cl_training_id' => $request['cl_training_id'],
              'cm_attestation_item_id' => $request['cm_attestation_item_id'],
              'mentor_id' => 2, // ДА БЪДЕ ОПРАВЕНО!
              'goal_owner_id' => 2, // ДА БЪДЕ ОПРАВЕНО!
              'created_by' => 2, // ДА БЪДЕ ОПРАВЕНО!
          ]);

          return $this->respond([

              'status' => 'success',
              'status_code' => Res::HTTP_CREATED,
              'message' => 'Успешно е добавена новата цел!',
              'goal' => $this->goalTransformer->transform($goal)
          ]);
      }
  }

  public function update(Request $request)
  {
      $rules = array (

          'title' => 'required',
      );

      $errors = '';

      $validator = Validator::make($request->all(), $rules);

      if ($validator-> fails()){

        $messages = $validator->errors()->getMessages();
        foreach($messages as $message){
          foreach($message as $msg){
            $errors .= '<p>'.$msg.'</p>';
          }
        }

        return $this->respondValidationError('Fields Validation Failed.', $errors);

      }

      else{

        $goal = CmGoal::find($request['goal_id'])
          ->update([
             'title' => $request['title'],
            'deadline' => $request['deadline'],
            
          ]);

          return $this->respond([

              'status' => 'success',
              'status_code' => Res::HTTP_OK,
              'message' => 'Успешно редактирахте целта!',
          ]);
      }
  }

  public function delete(Request $request){

    $id = $request['goal_id'];

    $user = CmGoal::find($id)->delete();

    return $this->respond([
        'status' => 'success',
        'status_code' => Res::HTTP_OK,
        'message' => 'Успешно изтрита Цел!',
    ]);

  }

}
