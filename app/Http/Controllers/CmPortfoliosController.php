<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Repository\Transformers\PortfolioTransformer;
use App\Http\Controllers\ApiController;
use \Illuminate\Http\Response as Res;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Models\CmPortfolio;
use App\Models\ClPortfolioTypes;

class CmPortfoliosController extends ApiController
{

     protected $portfolioTransformer;

  public function __construct(PortfolioTransformer $portfolioTransformer){

    $this->portfolioTransformer = $portfolioTransformer;

  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $limit = Input::get('limit') ?: 20;

    $portfolios = CmPortfolio::paginate($limit);

    return $this->respondWithPagination($portfolios, [
      'portfolios' => $this->portfolioTransformer->transformCollection($portfolios->all())
    ], 'Records Found!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $rules = array (

          'title' => 'required|max:255',
          'user_id' => 'required|max:255',
          'is_link' => 'required|max:255',
          'file' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/|required|max:255',
          
          'description' => 'required|max:255',
          'period' => 'required|max:255',
          'cl_portfolio_types_id' => 'required|max:255',
          'created_by' => 'required|max:255',
          'updated_by' => 'required|max:255',

      );

      $errors = '';

      $validator = Validator::make($request->all(), $rules);

      if ($validator-> fails()){

        $messages = $validator->errors()->getMessages();
        foreach($messages as $message){
          foreach($message as $msg){
            $errors .= '<p>'.$msg.'</p>';
          }
        }

        return $this->respondValidationError('Fields Validation Failed.', $errors);

      }

      else{


        $portfolio = CmPortfolio::create([

            'title' => $request['title'],
            
            'user_id' => $request['user_id'],
            'is_link' => $request['is_link'],
            'title' => $request['title'],
            'file' => $request['file'],
            'description' =>$request['description'],
            'period' => $request['period'],
            'study_type' => $request['study_type'],
            'cl_portfolio_types_id' => $request['cl_portfolio_types_id'],
            'created_by' => $request['user_id'],
            'updated_by' => $request['updated_by'],
              

          ]);

          return $this->respond([

              'status' => 'success',
              'status_code' => Res::HTTP_CREATED,
              'message' => 'Добавено Портфолио!',
              'portfolio' => $this->portfolioTransformer->transform($portfolio)
          ]);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          $portfolio = CmPortfolio::find($id);

          return $this->respond([
              'status' => 'success',
              'status_code' => Res::HTTP_OK,
              'portfolio' => $this->portfolioTransformer->transform($portfolio)
          ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    }
    public function update(Request $request)
    {
         $rules = array (



          'title' => 'required|min:5',


          'file' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/|required|max:255',

          'description' => 'required|min:3',

         

      );

      $errors = '';

      $validator = Validator::make($request->all(), $rules);

      if ($validator-> fails()){

        $messages = $validator->errors()->getMessages();
        foreach($messages as $message){
          foreach($message as $msg){
            $errors .= '<p>'.$msg.'</p>';
          }
        }

        return $this->respondValidationError('Fields Validation Failed.', $errors);

      }

      else{

        $portfolio = CmPortfolio::find($request['portfolio_id'])
          ->update([
             'title' => $request['title'],
            
            'user_id' => $request['user_id'],
            'is_link' => $request['is_link'],
            'title' => $request['title'],
            'file' => $request['file'],
            'description' =>$request['description'],
            'period' => $request['period'],
            'study_type' => $request['study_type'],
            'cl_portfolio_types_id' => $request['cl_portfolio_types_id'],
            'created_by' => $request['user_id'],
            'updated_by' => $request['updated_by'],
            
          ]);

          return $this->respond([

              'status' => 'success',
              'status_code' => Res::HTTP_OK,
              'message' => 'Успешно редактирахте Портфолиото!',
          ]);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request){

    $id = $request['portfolio_id'];

    $portfolio = CmPortfolio::find($id)->delete();

    return $this->respond([
        'status' => 'success',
        'status_code' => Res::HTTP_OK,
        'message' => 'Успешно изтрито Портфолио!',
    ]);

  }
}
