<?php

namespace App\Repository\Transformers;


class CmAttestationItemTransformer extends Transformer{

    public function transform($cm_attestation_item){
        return [
            'cm_attestation_item_id' => $cm_attestation_item->id,
            'cm_attestation_card_id' => $cm_attestation_item->cm_attestation_card_id,
            'cl_attestation_criterion_id' => $cm_attestation_item->cl_attestation_criterion_id,
            'cl_attestation_criterion_name' => $cm_attestation_item->clAttestationCriteria->name,
            'cl_attestation_criterion_max_pts' => $cm_attestation_item->clAttestationCriteria->max_points,
            'teacher_pts' => $cm_attestation_item->teacher_pts,
            'teacher_comments' => $cm_attestation_item->teacher_comments,
            'created_at' => \Carbon\Carbon::parse($cm_attestation_item->created_at)->format('Y-m-d')
        ];

    }

}
