<?php

namespace App\Repository\Transformers;


class PortfolioTransformer extends Transformer{

    public function transform($portfolio){
        return [
            'portfolio_id' => $portfolio->id,
            'user_id' => $portfolio->user_id,
            'is_link' => $portfolio->is_link,
            'title' => $portfolio->title,
            'file' => $portfolio->file,
            'description' => $portfolio->description,
            'period' => $portfolio->period,
            'study_type' => $portfolio->study_type,
            'cl_portfolio_types_id' => $portfolio->cl_portfolio_types_id,
            'created_by' => $portfolio->user_id,
           
        ];

    }

}