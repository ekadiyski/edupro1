<?php

namespace App\Repository\Transformers;


class ClAttestationCriteriasTransformer extends Transformer{

    public function transform($cm_attestation_criterias){
        return [
            'id' => $cm_attestation_criterias->id,
            'name' => $cm_attestation_criterias->name,
            'max_points' => $cm_attestation_criterias->max_points,
            'sequence' => $cm_attestation_criterias->sequence
        ];

    }

}
