<?php

namespace App\Repository\Transformers;


class GoalTransformer extends Transformer{

    public function transform($goal){
        return [
            'id' => $goal->id,
            'title' => $goal->title,
            // 'deadline' => \Carbon\Carbon::createFromFormat("Y-m-d", "2016-05-02 11:11:11"),
            'deadline' => \Carbon\Carbon::parse($goal->deadline)->format('Y-m-d'),
        ];

    }

}
