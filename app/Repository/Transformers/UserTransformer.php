<?php

namespace App\Repository\Transformers;


class UserTransformer extends Transformer{

    public function transform($user){
        return [
            'user_id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'phone_number' => $user->phone,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'user_type' => $user->user_type,
        ];

    }

}
