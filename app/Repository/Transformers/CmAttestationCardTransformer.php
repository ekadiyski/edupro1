<?php

namespace App\Repository\Transformers;


class CmAttestationCardTransformer extends Transformer{

    public function transform($cm_attestation_card){
        return [
            'cm_attestation_card_id' => $cm_attestation_card->id,
            'teacher_id' => $cm_attestation_card->teacher_id,
            'teacher_name' => $cm_attestation_card->teacher->first_name . ' ' . $cm_attestation_card->teacher->last_name,
            'mentor_id' => $cm_attestation_card->mentor_id,
            'mentor_name' => $cm_attestation_card->mentor->first_name  . ' ' . $cm_attestation_card->mentor->last_name,
            'created_at' => \Carbon\Carbon::parse($cm_attestation_card->created_at)->format('Y-m-d')
        ];

    }

}
