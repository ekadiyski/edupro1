import axios from "axios";
import { NotificationManager } from 'react-notifications';

export function fetchAttestationItemsByAttestationId(attestation_id){
  return function (dispatch) {
    axios.get(baseUrl+"api/v1/attestation-items/"+attestation_id)
    .then((response) => {
      dispatch({type: "FETCH_ATTESTATION_ITEMS_FULFILLED", payload: response.data});
    })
    .catch((error) => {
      dispatch({type: "FETCH_ATTESTATION_ITEMS_REJECTED", payload: error});
    })
  }
}
