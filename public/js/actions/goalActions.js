import axios from "axios";
import { NotificationManager } from 'react-notifications';

export function fetchGoals(){
  return function (dispatch) {
    axios.get(baseUrl+"api/v1/goals")
    .then((response) => {
      dispatch({type: "FETCH_GOALS_FULFILLED", payload: response.data});
    })
    .catch((error) => {
      dispatch({type: "FETCH_GOALS_REJECTED", payload: error});
    })
  }
}

export function fetchGoal(id){
  return function (dispatch) {
    axios.get(baseUrl+"api/v1/goals/"+id)
    .then((response) => {
      dispatch({type: "FETCH_GOAL_FULFILLED", payload: response.data.goal});
    })
    .catch((error) => {
      dispatch({type: "FETCH_GOAL_REJECTED", payload: error});
    })
  }
}


export function deleteGoal(formData){
  return function (dispatch) {
    axios.post(baseUrl+"api/v1/goal/delete", formData)
    .then((response) => {
      NotificationManager.success(response.data.message, 'Успех!', 5000);
      dispatch(fetchGoals());
    })
    .catch((error) => {
      NotificationManager.error("Грешка по време на операция!", 'Error', 5000);
    })
  }
}
