import axios from "axios";
import { NotificationManager } from 'react-notifications';

export function fetchAttestations(){
  return function (dispatch) {
    axios.get(baseUrl+"api/v1/attestations")
    .then((response) => {
      dispatch({type: "FETCH_ATTESTATIONS_FULFILLED", payload: response});
    })
    .catch((error) => {
      dispatch({type: "FETCH_ATTESTATIONS_REJECTED", payload: error});
    })
  }
}

export function fetchAttestation(id){
  return function (dispatch) {
    axios.get(baseUrl+"api/v1/attestations/"+id)
    .then((response) => {
      dispatch({type: "FETCH_ATTESTATION_FULFILLED", payload: response.data.attestation});
    })
    .catch((error) => {
      dispatch({type: "FETCH_ATTESTATION_REJECTED", payload: error});
    })
  }
}

export function deleteAttestation(formData){
  return function (dispatch) {
    axios.post(baseUrl+"api/v1/attestations/delete", formData)
    .then((response) => {
      NotificationManager.success(response.data.message, 'Успех!', 5000);
      dispatch(fetchAttestations());
    })
    .catch((error) => {
      NotificationManager.error("Грешка по време на операция!", 'Error', 5000);
    })
  }
}
