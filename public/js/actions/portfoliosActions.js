import axios from "axios";
import { NotificationManager } from 'react-notifications';

export function fetchPortfolios(){
  return function (dispatch) {
    axios.get(baseUrl+"api/v1/portfolio")
    .then((response) => {
      dispatch({type: "FETCH_PORTFOLIOS_FULFILLED", payload: response.data});
    })
    .catch((error) => {
      dispatch({type: "FETCH_PORTFOLIOS_REJECTED", payload: error});
    })
  }
}

export function fetchPortfolio(id){
  return function (dispatch) {
    axios.get(baseUrl+"api/v1/portfolio/"+id)
    .then((response) => {
      dispatch({type: "FETCH_PORTFOLIO_FULFILLED", payload: response.data.portfolio});
    })
    .catch((error) => {
      dispatch({type: "FETCH_PORTFOLIO_REJECTED", payload: error});
    })
  }
}


export function deletePortfolio(formData){
  return function (dispatch) {
    axios.post(baseUrl+"api/v1/portfolio/delete", formData)
    .then((response) => {
      NotificationManager.success(response.data.message, 'Успех!', 5000);
      dispatch(fetchPortfolios());
    })
    .catch((error) => {
      NotificationManager.error("Грешка по време на операция!", 'Error', 5000);
    })
  }
}
