import axios from "axios";
import { NotificationManager } from 'react-notifications';

export function fetchAttestationCriterias(){
  return function (dispatch) {
    axios.get(baseUrl+"api/v1/attestation-criterias")
    .then((response) => {
      dispatch({type: "FETCH_ATTESTATION_CRITERIAS_FULFILLED", payload: response.data});
    })
    .catch((error) => {
      dispatch({type: "FETCH_ATTESTATION_CRITERIAS_REJECTED", payload: error});
    })
  }
}
