export default function reducer(state={
    goals: [],
    goal: null,
    fetched: false,
    error: null
}, action){

  switch (action.type){

    case "FETCH_GOALS_FULFILLED": {
      return {
        ...state,
        fetched: true,
        goals: action.payload.data.goals,
      }
    }

    case "FETCH_GOALS_REJECTED": {
      return {
        ...state,
        fetched: false,
        error: action.payload
      }
    }

    case "FETCH_GOAL_REJECTED": {
      return {
        ...state,
        fetched: false,
        error: action.payload
      }
    }

    case "FETCH_GOAL_FULFILLED": {
      return {
        ...state,
        fetched: true,
        goal: action.payload
      }
    }
    
  }
  


    

  return state;

}
