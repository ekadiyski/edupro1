export default function reducer(state={
    attestation_criterias: [],
    fetched: false,
    error: null
}, action){

  switch (action.type){

    case "FETCH_ATTESTATION_CRITERIAS_REJECTED": {
      return {
        ...state,
        fetched: false,
        error: action.payload
      }
    }

    case "FETCH_ATTESTATION_CRITERIAS_FULFILLED": {
      return {
        ...state,
        fetched: true,
        attestation_criterias: action.payload.data.cl_attestation_criterias,
      }
    }
  }

  return state;

}
