export default function reducer(state={
    attestation_items: [],
    fetched: false,
    error: null
}, action){

  switch (action.type){

    case "FETCH_ATTESTATION_ITEMS_REJECTED": {
      return {
        ...state,
        fetched: false,
        error: action.payload
      }
    }

    case "FETCH_ATTESTATION_ITEMS_FULFILLED": {
      return {
        ...state,
        fetched: true,
        attestation_items: action.payload.cm_attestation_items,
      }
    }
  }

  return state;

}
