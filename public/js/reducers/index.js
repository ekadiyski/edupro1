import { combineReducers } from "redux"

import users from "./UsersReducer"
import goals from "./GoalsReducer"
import attestations from "./AttestationsReducer"
import portfolios from "./PortfoliosReducer"
import attestation_criterias from "./AttestationCriteriasReducer"
import attestation_items from "./AttestationItemsReducer"

export default combineReducers({
    users, goals, attestations, portfolios, attestation_criterias, attestation_items
})



