export default function reducer(state={
    portfolios: [],
    portfolio: null,
    fetched: false,
    error: null
}, action){

  switch (action.type){

    case "FETCH_PORTFOLIOS_REJECTED": {
      return {
        ...state,
        fetched: false,
        error: action.payload
      }
    }

    case "FETCH_PORTFOLIOS_FULFILLED": {
      return {
        ...state,
        fetched: true,
        portfolios: action.payload.data.portfolios,
      }
    }

    case "FETCH_PORTFOLIOS_REJECTED": {
      return {
        ...state,
        fetched: false,
        error: action.payload
      }
    }
 case "FETCH_PORTFOLIO_FULFILLED": {
      return {
        ...state,
        fetched: true,
        portfolio: action.payload
      }
    }

  }

  return state;

}
