export default function reducer(state={
    attestations: [],
    attestation: null,
    fetched: false,
    error: null
}, action){

  switch (action.type){

    case "FETCH_ATTESTATIONS_REJECTED": {
      return {
        ...state,
        fetched: false,
        error: action.payload
      }
    }

    case "FETCH_ATTESTATIONS_FULFILLED": {
      return {
        ...state,
        fetched: true,
        attestations: action.payload.data.cm_attestation_cards,
      }
    }

    case "FETCH_ATTESTATIONS_REJECTED": {
      return {
        ...state,
        fetched: false,
        error: action.payload
      }
    }

    case "FETCH_ATTESTATION_FULFILLED": {
      return {
        ...state,
        fetched: true,
        attestation: action.payload
      }
    }
  }

  return state;

}
