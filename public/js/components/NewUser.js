import React from 'react';
import { push } from "react-router";
﻿import axios from "axios";
import { NotificationManager } from 'react-notifications';

class NewUser extends React.Component{

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  constructor(){
    super();

    this.state = {
      errors: ''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    var formElement = document.querySelector("form");
    var formData = new FormData(formElement);
    axios.post(baseUrl+"api/v1/users/create", formData)
      .then((response) => {
        this.context.router.push('/users');
        NotificationManager.success('Добавихте потребител успешно!', 'Success', 5000);
      })
      .catch((error) => {
        var errors = error.response.data.data;
        this.setState({
          errors: errors
        });
        NotificationManager.error('Възникна грешка!', 'Error', 5000);
      });

  }

  createMarkup() {
    return {__html: this.state.errors};
  }


  render(){

    var errors = '';

    if(this.state.errors != ''){
      errors = <div class="alert alert-danger" role="alert"><div dangerouslySetInnerHTML={this.createMarkup()} /></div>
    }

    return(
              <div>
                <h1>Добави потребител</h1>
                <div className="col-lg-8">
                  {errors}
                  <form method="post" onSubmit={this.handleSubmit}>
                    <div className="form-group col-lg-6">
                      <input className="form-control" placeholder="Потребителско име" name="name"/>
                    </div>

                     <div className="form-group col-lg-6">
                      <input className="form-control" placeholder="Име" name="first_name"/>
                    </div>

                     <div className="form-group col-lg-6">
                      <input className="form-control" placeholder="Фамилия" name="last_name"/>
                    </div>

                    <div className="form-group col-lg-6">
                      <input className="form-control" placeholder="Имейл" name="email"/>
                    </div>

                    <div className="form-group col-lg-6">
                      <input type="password" className="form-control" placeholder="Парола" name="password"/>
                    </div>

                    <div className="form-group col-lg-6">
                      <input type="password" className="form-control" placeholder="Потвърди паролата" name="password_confirmation" />
                    </div>

                    <div className="form-group col-lg-6">
                      <input className="form-control" placeholder="Телефон" name="phone_number" />
                    </div>

                    <div className="form-group col-lg-6">
                      <button type="submit" className="btn btn-primary btn-block">Добави потребител</button>
                    </div>

                  </form>
                </div>

              </div>
          );
        }
  }

export default NewUser
