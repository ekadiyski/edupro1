import React from 'react';
import { connect } from "react-redux";
import { fetchGoal } from "./../actions/goalActions";
import { push } from "react-router";
﻿import axios from "axios";
import { NotificationManager } from 'react-notifications';

class EditGoal extends React.Component{
static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  constructor(props){
    super(props);

    this.state = {
      errors: '',
      goal_id: props.params.id,
    }

    this.handleSubmit = this.handleSubmit.bind(this);

  }

  handleSubmit(event) {
    event.preventDefault();

    var formElement = document.querySelector("form");
    var formData = new FormData(formElement);
    formData.append('goal_id', this.state.goal_id);

    axios.post(baseUrl+"api/v1/goals/update", formData)
      .then((response) => {

        this.context.router.push('/goals');
        NotificationManager.success('Успешно редактирахте целта!', 'Поздравления', 5000);

      })
      .catch((error) => {

        var errors = error.response.data.data;

        this.setState({
          errors: errors
        });

        NotificationManager.error('Възникна грешка с редактирането!', 'Грешка', 5000);
      });

  }

  componentWillMount(){
    this.props.dispatch(fetchGoal(this.state.goal_id));
  }

  createMarkup() {
    return {__html: this.state.errors};
  }

render(){
    const { goal } = this.props;

    var errors = '';

    if(this.state.errors != ''){
      errors = <div class="alert alert-danger" role="alert"><div dangerouslySetInnerHTML={this.createMarkup()} /></div>
    }

    var formElements = '';
    if(goal !== null){
      formElements =( <div>
         <div className="form-group col-lg-12">
                      <label>* Цел</label>
                      <input className="form-control" name="title" defaultValue={goal.title}/>
                    </div>

                     <div className="form-group col-lg-12">
                     <label>* Краен срок за изпълнение</label>
                      <input type="date" className="form-control" name="deadline" defaultValue={goal.deadline}/>
                    </div>

                    <div className="form-group col-lg-12">
                            <label>* Тип обучение</label>
                            <select className="form-control" name="cl_training_id">
                                <option value="1">Обучение 1</option>
                                <option value="2">Обучение 2</option>
                            </select>
                    </div>
                    <div className="form-group col-lg-12">
                            <label>* Препоръка от ментор (от атестацията)</label>
                            <select className="form-control" name="cm_attestation_item_id">
                                <option value="3">Препоръка 1</option>
                                <option value="3">Препоръка 2</option>
                                <option value="3">Препоръка 3</option>
                            </select>
                    </div>
                </div>
                );
    }
    
return(
            <div>
                <h1 className="text-center">Редактирай цел</h1>
            <div className="col-lg-6 col-lg-offset-3">
                  {errors}
                  <form method="post" onSubmit={this.handleSubmit}>

                    {formElements}

                    <div className="form-group col-lg-6">
                    <p><i>полетата с * са задължителни</i></p>
                      <button type="submit" className="btn btn-primary btn-block">Редактирай</button>
                    </div>

                  </form>
                </div>

              </div>
        );
}
}

function mapStateToProps(state) {
    console.log(state);
    return {
       goal: state.goals.goal,
    }
  }
  export default connect(mapStateToProps)(EditGoal)