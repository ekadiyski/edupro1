import React from 'react';
import { connect } from "react-redux";
import { Link } from "react-router"
import { fetchAttestations, deleteAttestation } from "./../actions/attestationActions";

class Attestations extends React.Component{

  constructor(){
    super();
    this.handleBtnDelete = this.handleBtnDelete.bind(this);
  }

  componentWillMount(){

    this.props.dispatch(fetchAttestations());

  }

  handleBtnDelete(id, event){
    event.preventDefault();

    var r = confirm("Сигурни ли сте че искате да изтриете тази Атестация?");
      if (r == true) {
        const url = baseUrl+"/api/v1/attestations/delete";
        var formElement = document.getElementById("form_"+id);
        var formData = new FormData(formElement);
        this.props.dispatch(deleteAttestation(formData));
      }
  }

  render(){

    return(
              <div>
                  <h1 className="pull-left">Атестации</h1>
                  <div className="col-lg-12">

                    <Link to="attestations/create" className="btn btn-primary btn-sm pull-left">Добави нова атестация &nbsp; <i className="glyphicon glyphicon-plus"></i></Link>

                    <table className="table table-responsive">

                      <thead>
                        <tr>
                          <th>Учител</th>
                          <th>Ментор</th>
                          <th>Дата на създаване</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        { this.props.attestations.map((attestation, index) => {
                            return (
                              <tr key={index+1}>
                                <td>{attestation.teacher_name}</td>
                                <td>{attestation.mentor_name}</td>
                                <td>{attestation.created_at}</td>
                                <td>
                                {attestation.cm_attestation_card_id}
                                  <Link to={'attestations/'+attestation.cm_attestation_card_id+'/edit'} className="btn btn-success btn-xs pull-left"><i className="glyphicon glyphicon-pencil"></i></Link>
                                  
                                    <form id={"form_"+attestation.cm_attestation_card_id} className="pull-left" method="post">
                                    <input type="hidden" name="cm_attestation_card_id" value={attestation.cm_attestation_card_id} />
                                    <a className="btn btn-danger btn-xs" onClick={(event) => this.handleBtnDelete(attestation.cm_attestation_card_id, event)} href="#" id={attestation.cm_attestation_card_id}><i className="glyphicon glyphicon-trash"></i></a>
                                  </form>
                                </td>
                              </tr>
                            )
                          }) }
                      </tbody>

                    </table>

                  </div>
              </div>
          );
        }
  }

  function mapStateToProps(state) {
    return {
      attestations: state.attestations.attestations,
    }
  }
  export default connect(mapStateToProps)(Attestations)
