import React from 'react';
import { push } from "react-router";
import { connect } from "react-redux";
﻿import axios from "axios";
import { NotificationManager } from 'react-notifications';
import { fetchAttestationItemsByAttestationId } from "./../actions/attestationItemActions";

class EditAttestation extends React.Component{

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  constructor(props){
    super(props);

    this.state = {
      errors: '',
      attestation_id: props.params.attestation_id
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount(){
    this.props.dispatch(fetchAttestationItemsByAttestationId(this.state.attestation_id));
  }

  handleSubmit(event) {
    event.preventDefault();

    var formElement = document.querySelector("form");
    var formData = new FormData(formElement);
    formData.append('attestation_id', this.state.attestation_id);
    
    axios.post(baseUrl+"api/v1/attestations/update", formData)
      .then((response) => {
        this.context.router.push('/attestations');
        NotificationManager.success('Обновихте атестация!', 'Успех', 5000);
      })
      .catch((error) => {
        var errors = error.response.data.data;
        this.setState({
          errors: errors
        });
        NotificationManager.error('Възникна грешка!', 'Грешка', 5000);
      });

  }

  createMarkup() {
    return {__html: this.state.errors};
  }


  render(){

    var errors = '';

    if(this.state.errors != ''){
      errors = <div class="alert alert-danger" role="alert"><div dangerouslySetInnerHTML={this.createMarkup()} /></div>
    }

    return(
              <div>
                <h1>Атестация</h1>
                <div className="col-lg-8">
                  {errors}
                  <form method="post" onSubmit={this.handleSubmit}>
                    <table className="table table-responsive">
                        <thead>
                          <tr>
                            <th>Критерии</th>
                            <th>Максимален брой точки</th>
                            <th>Самооценка</th>
                            <th>Коментар</th>
                          </tr>
                        </thead>
                        <tbody>
                         
                            { this.props.attestation_items.map((attestation_item, index) => {
                                    return (
                                      <tr key={index+1}>
                                        <td>*{attestation_item.cl_attestation_criterion_name}</td>
                                        <td>{attestation_item.cl_attestation_criterion_max_pts}</td>
                                        <td>
                                            <input type="hidden" className="form-control" placeholder="" name="cl_attestation_criterion_id[]" value={attestation_item.id}/>

                                            <input className="form-control" placeholder="" name="teacher_pts[]" type="number" min="0" max={attestation_item.cl_attestation_criterion_max_pts} defaultValue={attestation_item.teacher_pts}/>
                                        </td>
                                        <td>
                                            <textarea rows="4" cols="50" name="teacher_comments[]" form="attesttion" defaultValue={attestation_item.teacher_comments}>
                                        </textarea>
                                        </td>
                                      </tr>
                                    )
                                  }) }

                            
                        </tbody>
                    </table>
                    <p><i>полетата с * са задължителни</i></p>
                    <div className="form-group col-lg-6">
                      <button type="submit" className="btn btn-primary btn-block">Редактирай атестационна карта</button>
                    </div>
                  </form>
                </div>

              </div>
          );
        }
  }

  function mapStateToProps(state) {
    return {
      attestation_items: state.attestation_items.attestation_items,
    }
  }
  export default connect(mapStateToProps)(EditAttestation)
