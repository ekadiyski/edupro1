import React from 'react';
import { connect } from "react-redux";
import { fetchPortfolio } from "./../actions/portfoliosActions";
import { push } from "react-router";
﻿import axios from "axios";
import { NotificationManager } from 'react-notifications';

class EditPortfolio extends React.Component{
static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  constructor(props){
    super(props);

    this.state = {
      errors: '',
      portfolio_id: props.params.id,
    }

    this.handleSubmit = this.handleSubmit.bind(this);

  }

  handleSubmit(event) {
    event.preventDefault();

    var formElement = document.querySelector("form");
    var formData = new FormData(formElement);
    formData.append('portfolio_id', this.state.portfolio_id);

    axios.post(baseUrl+"api/v1/portfolio/update", formData)
      .then((response) => {

        this.context.router.push('/portfolio');
        NotificationManager.success('Успешно редактирахте целта!', 'Поздравления', 5000);

      })
      .catch((error) => {

        var errors = error.response.data.data;

        this.setState({
          errors: errors
        });

        NotificationManager.error('Възникна грешка с редактирането!', 'Грешка', 5000);
      });

  }

  componentWillMount(){
    this.props.dispatch(fetchPortfolio(this.state.portfolio_id));
  }

  createMarkup() {
    return {__html: this.state.errors};
  }

render(){
    const { portfolio } = this.props;

    var errors = '';

    if(this.state.errors != ''){
      errors = <div class="alert alert-danger" role="alert"><div dangerouslySetInnerHTML={this.createMarkup()} /></div>
    }

    var formElements = '';
    if(portfolio !== null){
      formElements =( <div>
          <div className="form-group col-lg-6">
          Заглавие
          <input className="form-control"  name="title" defaultValue={portfolio.title}/>
          </div>

          <div className="form-group col-lg-6">
          Линк
          <input className="form-control"  name="file" defaultValue={portfolio.file}/>
          </div>

          <div className="form-group col-lg-6">
          <input type="hidden" className="form-control" name="user_id" value="2"/>
          </div>

          <div className="form-group col-lg-6">
          <input type="hidden" className="form-control" name="created_by" value="2"/>
          </div>

          <div className="form-group col-lg-6">
          <input type="hidden" className="form-control" name="updated_by" value="0"/>
          </div>

          <div className="form-group col-lg-6">
          <input type="hidden" className="form-control" name="is_link" value="1"/>
          </div>

         

          

          <div className="form-group col-lg-6">
          <label>
          
          Избери тип на Портфолио
          <select name="cl_portfolio_types_id" value={this.state.value} onChange={this.handleChange} defaultValue={portfolio.cl_portfolio_types_id}>
          
   
              <option value="1">Дипломи за висше образование</option>
              <option value="2">Сертификати от обучения</option>
              <option value="3">Грамота от олимпиада</option>
              <option value="4">Награда</option>
         

          </select>
          </label>
          </div>
          <div className="form-group col-lg-6">
          Описание
          <input className="form-control"  name="description" defaultValue={portfolio.description}/>
          </div>

          <div className="form-group col-lg-6">
          <label>
          Избери Период
          <select name="period" value={this.state.value} onChange={this.handleChange} defaultValue={portfolio.period}>
          <option selected="true" disabled="disabled" value=""> </option>
          <option value="1 Месец">1 Месец</option>
          <option value="3 Месеца">2 Месец</option>
          <option value="6 Месеца">3 Месец</option>
          <option value="9 Месеца">4 Месец</option>
          <option value="" disabled="disabled">---</option>
          <option value="1 Година">1 Година</option>
          <option value="3 Години">3 Години</option>
          <option value="6 Години">6 Години</option>
          <option value="9 Години">9 Години</option>
          </select>
          </label>
          </div>

          <div className="form-group col-lg-6">
          Вид на обучение
          <input className="form-control"  name="study_type" defaultValue={portfolio.study_type}/>
          </div>




                    
                </div>
                );
    }
    
return(
            <div>
                <h1 className="text-center">Редактирай Портфолио</h1>
            <div className="col-lg-6 col-lg-offset-3">
                  {errors}
                  <form method="post" onSubmit={this.handleSubmit}>

                    {formElements}

                    <div className="form-group col-lg-6">
                      <button type="submit" className="btn btn-primary btn-block">Редактирай</button>
                    </div>

                  </form>
                </div>

              </div>
        );
}
}

function mapStateToProps(state) {
    console.log(state);
    return {
       portfolio: state.portfolios.portfolio,
    }
  }
  export default connect(mapStateToProps)(EditPortfolio)