import React from 'react';
import { connect } from "react-redux";
import { Link } from "react-router"
import { fetchGoals } from "./../actions/goalActions";

class Goals extends React.Component{
  constructor(){
    super();
    
  }
  componentWillMount(){

    this.props.dispatch(fetchGoals());

  }
render(){
return(
            <div>
                <h1>Цели</h1>
                <Link to="goals/create" className="btn btn-primary btn-sm pull-left">Добави нова цел &nbsp; <i className="glyphicon glyphicon-plus"></i></Link>
                <table className="table table-responsive">

                      <thead>
                        <tr>
                          <th>#id</th>
                          <th>Title</th>
                        </tr>
                      </thead>
                      <tbody>
                        { this.props.goals.map((goal, index) => {
                            return (
                              <tr key={index+1}>
                                <td>{goal.id}</td>
                                <td>{goal.title}</td>
                              </tr>
                            )
                          }) }
                      </tbody>

                    </table>
            </div>
        );
}
}

  
  function mapStateToProps(state) {
    return {
      goals: state.cm_goals,
    }
    
  }
  
  export default connect(mapStateToProps)(Goals)