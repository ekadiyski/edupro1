import React from 'react';
import { push } from "react-router";
import { connect } from "react-redux";
﻿import axios from "axios";
import { NotificationManager } from 'react-notifications';
import { fetchAttestationCriterias } from "./../actions/attestationCriteriaActions";

class NewAttestation extends React.Component{

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  constructor(){
    super();

    this.state = {
      errors: ''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount(){
    this.props.dispatch(fetchAttestationCriterias());
  }

  handleSubmit(event) {
    event.preventDefault();

    var formElement = document.querySelector("form");
    var formData = new FormData(formElement);
    axios.post(baseUrl+"api/v1/attestations", formData)
      .then((response) => {
        this.context.router.push('/attestations');
        NotificationManager.success('Добавихте атестация!', 'Успех', 5000);
      })
      .catch((error) => {
        var errors = error.response.data.data;
        this.setState({
          errors: errors
        });
        NotificationManager.error('Възникна грешка!', 'Грешка', 5000);
      });

  }

  createMarkup() {
    return {__html: this.state.errors};
  }


  render(){

    var errors = '';

    if(this.state.errors != ''){
      errors = <div class="alert alert-danger" role="alert"><div dangerouslySetInnerHTML={this.createMarkup()} /></div>
    }

    return(
              <div>
                <h1>Атестация</h1>
                <div className="col-lg-8">
                  {errors}
                  <form id="attesttion" method="post" onSubmit={this.handleSubmit}>
                    <table className="table table-responsive">
                        <thead>
                          <tr>
                            <th>Критерии</th>
                            <th>Максимален брой точки</th>
                            <th>Самооценка</th>
                            <th>Коментар</th>
                          </tr>
                        </thead>
                        <tbody>

                            { this.props.attestation_criterias.map((attestation_criteria, index) => {
                                    return (
                                      <tr key={index+1}>
                                        <td>* {attestation_criteria.name}</td>
                                        <td>{attestation_criteria.max_points}</td>
                                        <td>
                                            <input type="hidden" className="form-control" placeholder="" name="cl_attestation_criterion_id[]" value={attestation_criteria.id}/>
                                            <input className="form-control" placeholder="" name="teacher_pts[]" type="number" min="0" max={attestation_criteria.max_points}/>

                                        </td>
                                        <td>
                                        <textarea rows="4" cols="50" name="teacher_comments[]" form="attesttion">
                                        </textarea>
                                           
                                        </td>
                                      </tr>
                                    )
                                  }) }

                            
                        </tbody>
                    </table>
                    <p><i>полетата с * са задължителни</i></p>
                    <div className="form-group col-lg-6">
                      <button type="submit" className="btn btn-primary btn-block">Създай атестационна карта</button>
                    </div>
                  </form>
                </div>

              </div>
          );
        }
  }

  function mapStateToProps(state) {
    return {
      attestation_criterias: state.attestation_criterias.attestation_criterias,
    }
  }
  export default connect(mapStateToProps)(NewAttestation)
