import React from "react";
import { IndexLink, Link } from "react-router";
export default class Navigation extends React.Component{
    constructor(){
        super();
        this.state = {
            collapsed: true,
        };
    }
toggleCollapse(){
        const collapsed =!this.state.collapsed;
        this.setState({collapsed});
    }
render(){
        const { location } = this.props;
const {collapsed } = this.state;
        const homeClass = location.pathname === '/' ? "active" : "";
        const usersClass = location.pathname.match(/^\/users/) ? "active" : "";
        const portfolioClass = location.pathname.match(/^\/portfolio/) ? "active" : "";
        const attestationsClass = location.pathname.match(/^\/attestations/) ? "active" : "";
        const goalsClass = location.pathname.match(/^\/goals/) ? "active" : "";
        const navClass = collapsed ? "collapse" : "";
return(
<div>
    <header class="main-header">
        <Link to="portfolio" onClick={this.toggleCollapse.bind(this)} class="logo">
            <span class="logo-mini"><b>A</b>LT</span>
            <span class="logo-lg"><b>Admin</b>LTE</span>
        </Link>

        <nav class="navbar navbar-static-top" role="navigation">
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                          <i class="fa fa-bell-o"></i>
                          <span class="label label-warning">10</span>
                        </a>
                        <ul class="dropdown-menu">
                          <li class="header">You have 10 notifications</li>
                          <li>
                            <ul class="menu">
                              <li>
                                <a href="#">
                                  <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                </a>
                              </li>
                            </ul>
                          </li>
                          <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <div class="main-sidebar">
        <div class="sidebar">
          <ul class="sidebar-menu">
            <li class={usersClass}>
                <Link to="users" onClick={this.toggleCollapse.bind(this)}>Потребители</Link>
            </li>
            <li class={portfolioClass}>
                <Link to="portfolio" onClick={this.toggleCollapse.bind(this)}>Портфолио</Link>
            </li>
            <li class={attestationsClass}>
                <Link to="attestations" onClick={this.toggleCollapse.bind(this)}>Атестации</Link>
            </li>
            <li class={goalsClass} class="treeview">
                <Link to="goals" onClick={this.toggleCollapse.bind(this)}>Цели</Link>
            </li>
          </ul>
        </div>
    </div>
</div>
);
}
}
