import React from 'react';
import { connect } from "react-redux";
import { Link } from "react-router"
import { fetchGoals, deleteGoal } from "./../actions/goalActions";

class Goals extends React.Component{

  constructor(){
    super();
    this.handleBtnDelete = this.handleBtnDelete.bind(this);
  }

  componentWillMount(){

    this.props.dispatch(fetchGoals());

  }

  handleBtnDelete(id, event){
    event.preventDefault();

    var r = confirm("Are you sure you want to delete this document!");
      if (r == true) {
        const url = baseUrl+"/api/v1/goal/delete";
        var formElement = document.getElementById("form_"+id);
        var formData = new FormData(formElement);
        this.props.dispatch(deleteGoal(formData));
      }
  }

  render(){

    return(
              <div>
                  <h1 className="pull-left">Активни цели</h1>
                  <div className="col-lg-12">
                  <Link to="goals/create" className="btn btn-primary btn-sm pull-left">Добави нова цел &nbsp; <i className="glyphicon glyphicon-plus"></i></Link>
                    <table className="table table-responsive">

                      <thead>
                        <tr>
                          <th>Описание</th>
                          <th>Краен срок</th>
                          <th>Опции</th>
                        </tr>
                      </thead>
                      <tbody>
                        { this.props.goals.map((goal, index) => {
                            return (
                              <tr key={index+1}>
                                <td>{goal.title}</td>
                                <td>{goal.deadline}</td>
                                <td><Link to={'goals/'+goal.id+'/edit'} className="btn btn-success btn-xs pull-left"><i className="glyphicon glyphicon-pencil"></i></Link>
                                 <form id={"form_"+goal.id} className="pull-left" method="post">
                                    <input type="hidden" name="goal_id" value={goal.id} />
                                    <a className="btn btn-danger btn-xs" onClick={(event) => this.handleBtnDelete(goal.id, event)} href="#" id={goal.id}><i className="glyphicon glyphicon-trash"></i></a>
                                  </form>
                                </td>
                                
                              </tr>
                            )
                          }) }
                      </tbody>

                    </table>

                  </div>
              </div>
          );
        }
  }

  function mapStateToProps(state) {
    console.log(state);
    return {

      goals: state.goals.goals,
    }
  }
  export default connect(mapStateToProps)(Goals)
