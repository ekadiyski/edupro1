import React from 'react';
import { Link } from "react-router";
import { push } from "react-router";
﻿import axios from "axios";
import { NotificationManager } from 'react-notifications';

export default class NewGoal extends React.Component{
    static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  constructor(){
    super();

    this.state = {
      errors: ''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    var formElement = document.querySelector("form");
    var formData = new FormData(formElement);
    axios.post(baseUrl+"api/v1/goals", formData)
      .then((response) => {
        this.context.router.push('/goals');
        NotificationManager.success('Успешно е добавена нова цел!', 'Поздравления!', 5000);
      })
      .catch((error) => {
        var errors = error.response.data.data;
        this.setState({
          errors: errors
        });
        NotificationManager.error('Възникна грешка!', 'Грешка!', 5000);
      });

  }

  createMarkup() {
    return {__html: this.state.errors};
  }
render(){
    var errors = '';

    if(this.state.errors != ''){
      errors = <div class="alert alert-danger" role="alert"><div dangerouslySetInnerHTML={this.createMarkup()} /></div>
    }
return(

            <div>
                <h1 className="text-center">Добавяне на нова цел</h1>
                <div className="col-lg-6 col-lg-offset-3">
                  {errors}
                  <form method="post" onSubmit={this.handleSubmit}>
                    <div className="form-group col-lg-12">
                      <label>* Цел</label>
                      <input className="form-control" name="title" />
                    </div>

                     <div className="form-group col-lg-12">
                     <label>* Краен срок за изпълнение</label>
                      <input type="date" className="form-control" name="deadline" />
                    </div>

                    <div className="form-group col-lg-12">
                            <label>* Тип обучение</label>
                            <select className="form-control" name="cl_training_id">
                                <option value="1">Обучение 1</option>
                                <option value="2">Обучение 2</option>
                            </select>
                    </div>
                    <div className="form-group col-lg-12">
                            <label>* Препоръка от ментор (от атестацията)</label>
                            <select className="form-control" name="cm_attestation_item_id">
                                <option value="3">Препоръка 1</option>
                                <option value="3">Препоръка 2</option>
                                <option value="3">Препоръка 3</option>
                            </select>
                    </div>
                    <p><i>полетата с * са задължителни</i></p>
                    <div className="form-group col-lg-12">
                      <button type="submit" className="btn btn-primary btn-block">Добави</button>
                    </div>

                  </form>
                </div>

              </div>
        );
}
}
