import React from 'react';
import { connect } from "react-redux";
import { Link } from "react-router"
import { fetchPortfolios, deletePortfolio } from "./../actions/portfoliosActions";

class Portfolios extends React.Component{

  constructor(){
    super();
    this.handleBtnDelete = this.handleBtnDelete.bind(this);
  }

  componentWillMount(){

    this.props.dispatch(fetchPortfolios());

  }

  handleBtnDelete(id, event){
    event.preventDefault();

    var r = confirm("Сигурни ли сте че искате да изтриете това Портфолио ?");
      if (r == true) {
        const url = baseUrl+"/api/v1/portfolios/delete";
        var formElement = document.getElementById("form_"+id);
        var formData = new FormData(formElement);
        this.props.dispatch(deletePortfolio(formData));
      }
  }

  render(){

    return(
              <div>
                  <h1 className="pull-left">Портфолио</h1>
                  <div className="col-lg-12">

                    <Link to="portfolio/create" className="btn btn-primary btn-sm pull-left">Добави ново портфолио &nbsp; <i className="glyphicon glyphicon-plus"></i></Link>

                    <table className="table table-responsive">

                      <thead>
                        <tr>
                          <th>Заглавие</th>
                          <th>Файл</th>
                          <th>Описание</th>
                          <th>Период</th>
                          <th>Вид на обучение</th>
                          <th>Вид на портфолио</th>
                       
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        { this.props.portfolios.map((portfolio, index) => {
                            return (
                              <tr key={index+1}>
                                <td>{portfolio.title}</td>
                                <td>{portfolio.file}</td>
                                <td>{portfolio.description}</td>
                                <td>{portfolio.period}</td>
                                <td>{portfolio.study_type}</td>
                                <td>{portfolio.cl_portfolio_types_id}</td>
                               
                                <td>

                                  <Link to={'portfolio/'+portfolio.portfolio_id+'/edit'} className="btn btn-success btn-xs pull-left"><i className="glyphicon glyphicon-pencil"></i></Link>

                                  <form id={"form_"+portfolio.portfolio_id} className="pull-left" method="post">
                                    <input type="hidden" name="portfolio_id" value={portfolio.portfolio_id} />
                                    <a className="btn btn-danger btn-xs" onClick={(event) => this.handleBtnDelete(portfolio.portfolio_id, event)} href="#" id={portfolio.portfolio_id}><i className="glyphicon glyphicon-trash"></i></a>
                                  </form>
                                </td>
                              </tr>
                            )
                          }) }
                      </tbody>

                    </table>

                  </div>
              </div>
          );
        }
  }

  function mapStateToProps(state) {
    return {
      portfolios: state.portfolios.portfolios,
    }
  }
  export default connect(mapStateToProps)(Portfolios)
