import React from 'react';
import { push } from "react-router";
﻿import axios from "axios";
import { NotificationManager } from 'react-notifications';

class NewPortfolio extends React.Component{

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  constructor(){
    super();

    this.state = {
      errors: ''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    var formElement = document.querySelector("form");
    var formData = new FormData(formElement);
    axios.post(baseUrl+"api/v1/portfolio", formData)
      .then((response) => {
        this.context.router.push("/portfolio");
        NotificationManager.success('Добавено Портфолио успешно!', 'Success', 5000);
      })
      .catch((error) => {
        var errors = error.response.data.data;
        this.setState({
          errors: errors
        });
        NotificationManager.error('Грешка по време на Операция!', 'Error', 5000);
      });

  }

  createMarkup() {
    return {__html: this.state.errors};
  }


  render(){

    var errors = '';

    if(this.state.errors != ''){
      errors = <div class="alert alert-danger" role="alert"><div dangerouslySetInnerHTML={this.createMarkup()} /></div>
    }

    return(
              <div>
                <h1>Създай Портфолио</h1>
                
                <div className="col-lg-8">
                  {errors}
                  <form method="post" onSubmit={this.handleSubmit}>
                    <div className="form-group col-lg-6">
                    * Заглавие
                      <input className="form-control" placeholder="заглавие" name="title"/>
                    </div>

                    <div className="form-group col-lg-6">
                    * Линк
                      <input className="form-control" placeholder="Линк към файл" name="file"/>
                    </div>

                     <div className="form-group col-lg-6">
                      <input type="hidden" className="form-control" name="user_id" value="2"/>
                    </div>

                    <div className="form-group col-lg-6">
                      <input type="hidden" className="form-control" name="created_by" value="2"/>
                    </div>

                    <div className="form-group col-lg-6">
                      <input type="hidden" className="form-control" name="updated_by" value="0"/>
                    </div>

                    <div className="form-group col-lg-6">
                      <input type="hidden" className="form-control" name="is_link" value="1"/>
                    </div>

                    <div className="form-group col-lg-6">
                      
                        <label>
          * Избери тип на Портфолио
          <select name="cl_portfolio_types_id" value={this.state.value} onChange={this.handleChange}>
          
            <option value="1">Дипломи за висше образование</option>
            <option value="2">Сертификати от обучения</option>
            <option value="3">Грамота от олимпиада</option>
            <option value="4">Награда</option>
           
          </select>
        </label>
                    </div>
                    
                     <div className="form-group col-lg-6">
                     * Описание
                      <input className="form-control" placeholder="описание" name="description"/>
                    </div>

                     <div className="form-group col-lg-6">
                       <label>
          * Избери Период
          <select name="period" value={this.state.value} onChange={this.handleChange}>
          <option selected="true" disabled="disabled" value=""> </option>
            <option value="1 Месец">1 Месец</option>
            <option value="3 Месеца">2 Месец</option>
            <option value="6 Месеца">3 Месец</option>
            <option value="9 Месеца">4 Месец</option>
            <option value="" disabled="disabled">---</option>
            <option value="1 Година">1 Година</option>
            <option value="3 Години">3 Години</option>
            <option value="6 Години">6 Години</option>
            <option value="9 Години">9 Години</option>
          </select>
        </label>
                    </div>

                    <div className="form-group col-lg-6">
                    Вид на обучение
                      <input className="form-control" placeholder="вид на обучение" name="study_type"/>
                    </div>
        

                  <p><i>полетата с * са задължителни</i></p>
                    <div className="form-group col-lg-6">
                      <button type="submit" className="btn btn-primary btn-block">Създай Портфолио!</button>
                    </div>

                  </form>
                </div>

              </div>
          );
        }
  }

export default NewPortfolio