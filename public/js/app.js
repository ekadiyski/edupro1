import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from "react-router";
import { Provider } from "react-redux";
import Layout from "./components/Layout";
import Home from "./components/Home";
import Users from "./components/Users";
import NewUser from "./components/NewUser";
import EditUser from "./components/EditUser";
import Portfolios from "./components/Portfolios";
import NewPortfolios from "./components/NewPortfolios";
import EditPortfolio from "./components/EditPortfolio";
import Attestations from "./components/Attestations";
import NewAttestation from "./components/NewAttestation";
import EditAttestation from "./components/EditAttestation";
import Goals from "./components/Goals";
import NewGoal from "./components/NewGoal";
import EditGoal from "./components/EditGoal";

import store from "./store";

const app = document.getElementById('app');

ReactDOM.render(
  <Provider store={store}>
    <Router history = { hashHistory }>
      <Route path = "/" component = { Layout }>
        <IndexRoute component = { Portfolios }></IndexRoute>
        <Route path = "users" component = { Users }></Route>
        <Route path = "users/new" component = { NewUser }></Route>
        <Route path = "users/:id/edit" component = { EditUser }></Route>
        <Route path = "portfolio" component = { Portfolios }></Route>
        <Route path = "portfolio/create" component = { NewPortfolios }></Route>
        <Route path = "portfolio/:id/edit" component = { EditPortfolio }></Route>
        <Route path = "attestations" component = { Attestations }></Route>
        <Route path = "attestations/create" component = { NewAttestation }></Route>
        <Route path = "attestations/:attestation_id/edit" component = { EditAttestation }></Route>
        <Route path = "goals" component = { Goals }></Route>
        <Route path = "goals/create" component = { NewGoal }></Route>
        <Route path = "goals/:id/edit" component = { EditGoal }></Route>
      </Route>
    </Router>
  </Provider>,
    app);
