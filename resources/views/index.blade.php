<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EduPro2</title>
    <!-- Bootstrap Core CSS -->
    <!--<link href="https://bootswatch.com/cosmo/bootstrap.css" rel="stylesheet">-->
    <link href="{{url('')}}/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="{{url('')}}/css/notification.css">

    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{url('')}}/admin-lte/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('')}}/admin-lte/dist/css/AdminLTE.min.css">

    <link rel="stylesheet" href="{{url('')}}/admin-lte/dist/css/skins/_all-skins.min.css">

    <script> var baseUrl = "{{url('')}}/";
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div id="app"></div>
<script src="{{url('')}}/js/bundle.js"></script>

<!-- jQuery 2.2.3 -->
<script src="{{url('')}}/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{url('')}}/admin-lte/bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="{{url('')}}/admin-lte/dist/js/app.min.js"></script>
</body>
</html>
